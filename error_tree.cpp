#include <cmath>
#include <queue>
#include <string>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <regex>

using namespace std;

class Node
{
    public:
        char val;
        Node *parent;
        Node *left_child;
        Node *right_child;
};

string PrintSExpression(Node* node)
{
    
    if (node != nullptr)
    {
        return string("(") + node->val + PrintSExpression(node->left_child) + PrintSExpression(node->right_child) + string(")");
    }
    else
    {
        return "";
    }
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    string in;
    getline(cin, in);
    if (in.length() == 0)
    {
        return 0;
    }
    regex b("\\([a-zA-Z],[a-zA-Z]\\)");

    unordered_map<char, Node*> node_map;
    unordered_map<char, Node*> roots;

    unsigned str_index = 0;
    while (str_index < in.length() - 3)
    {   
        string group = in.substr(str_index, 5);
        if (!regex_match(group, b) || (str_index + 5 < in.length() && in.at(str_index + 5) != ' ') || in.back() == ' ')
        {
            cout << "E1";
            return 0;
        }

        char parent_val = in.at(str_index + 1);
        char child_val = in.at(str_index + 3);
        str_index += 6;

        Node *node_parent;
        Node *node_child;

        if (node_map.find(parent_val) == node_map.end())
        {
            node_parent = new Node();
            node_parent->val = parent_val; 
            node_map[parent_val] = node_parent;           
        }
        else
        {
            node_parent = node_map[parent_val];
        }
    
        if (node_map.find(child_val) == node_map.end())
        {
            node_child = new Node();
            node_child->val = child_val;
            node_map[child_val] = node_child;
        }
        else
        {
            node_child = node_map[child_val];
        }

        // add parent to node
        node_child->parent = node_parent;

        // add child to node, shift children to keep order
        if (node_parent->left_child == nullptr)
        {
            // no children
            node_parent->left_child = node_child;
        }
        else if (node_parent->right_child == nullptr)
        {   
            // one child
            if (child_val < node_parent->left_child->val)
            {
                node_parent->right_child = node_parent->left_child;
                node_parent->left_child = node_child; 
            }
            else if (child_val > node_parent->left_child->val)
            {
                node_parent->right_child = node_child;
            }
            else
            {
                cout << "E2";
                 return 0;
            }
        }
        else
        {
            // full with 2 children
            if (node_parent->right_child->val == child_val || node_parent->left_child->val == child_val)
            {
                cout << "E2";
                return 0;
            }
            else
            {
                cout << "E3";
                return 0;
            }
        }

        //add potential roots and delete nodes that now have parents
        if (node_parent->parent == nullptr)
        {
            roots[parent_val] = node_parent;
        }
        // remove child from potential roots
        roots.erase(child_val);
    }
    // root node has a cycle
    if (roots.size() == 0)
    {
        cout << "E5";
        return 0;
    }
    // multiple roots
    else if (roots.size() > 1)
    {
        cout << "E4";
        return 0;
    }

    // BFS to find any cycles
    unordered_set<char> visited;
    queue<Node*> nodes;
    nodes.push(roots.begin()->second);
    
    while (nodes.size() > 0)
    {
        Node* node = nodes.front();
        pair<unordered_set<char>::iterator, bool> ptr;
        ptr = visited.insert(node->val);
        if (!ptr.second)
        {
            cout << "E5";
            return 0;
        }
        nodes.pop();

        if (node->left_child != nullptr)
        {
            nodes.push(node->left_child);
        }
        if (node->right_child != nullptr)
        {
            nodes.push(node->right_child);
        }

    }
    cout << PrintSExpression(roots.begin()->second);
    return 0;
}